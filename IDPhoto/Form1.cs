﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace IDPhoto
{
    public partial class Form1 : Form
    {

        string printerName = "";

        int res = 10;
        const int wFormat = 100;
        const int hFormat = 150;

        int hCount = 0;
        int wCount = 0;

        int wantedWidth = 35;
        int wantedHeight = 45;

        Image idPhoto;
        Image loadedImage;
        string filePath = null;
        string logPath = string.Format("{0}\\IDlogs\\", Path.GetTempPath());
        public Form1()
        {
            InitializeComponent();
            if (!Directory.Exists(logPath))
            {
                try
                {

                    Directory.CreateDirectory(logPath);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("File writing failed. \n Error: \n {0}{1}",ex.Message,logPath));
                    this.Dispose();
                    this.Close();
                }
                
            }

            widthText.Text = "35";
            heightText.Text = "45";
            string defaultPrinter = getDefaultPrinter();
            ListPrinters(defaultPrinter);
        }

        //public extern void  generateRectangles();

        Image cropImage()
        {
            Bitmap bmpImage = new Bitmap(Image.FromFile(pathText.Text));
            loadedImage = (Image)bmpImage;
            Bitmap bmpCrop = bmpImage.Clone(generateCropRect(), bmpImage.PixelFormat);
            loadedImage = (Image)bmpCrop;

            Bitmap finalEdit = new Bitmap(wFormat * res, hFormat * res, loadedImage.PixelFormat);

            List<int> posList = positionImages();

            Graphics g = Graphics.FromImage((Image)finalEdit);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            for (int i = 0; i < posList.Count; i += 2)
                g.DrawImage(loadedImage, posList[i] * res, posList[i + 1] * res, (wantedWidth) * res, (wantedHeight) * res);

            g.Dispose();

            

            
            return (Image)finalEdit;
        }

        Rectangle generateCropRect()
        {
            float wantedRatio = (float)wantedWidth / wantedHeight;
            float imageRatio = (float)loadedImage.Width / loadedImage.Height;
            Rectangle cropRect = new Rectangle();

            float x = 0;
            float y = 0;
            float w = loadedImage.Width;
            float h = loadedImage.Height;

            if (imageRatio < wantedRatio) //width crop
            {

                h = w * (float)wantedHeight / wantedWidth;
                y = (float)(loadedImage.Height - h) / 2;

            }
            else if (imageRatio > wantedRatio)//height crop
            {

                w = h * (float)wantedWidth / wantedHeight;
                x = (float)(loadedImage.Width - w) / 2;
            }

            cropRect.X = (int)x;
            cropRect.Y = (int)y;
            cropRect.Width = (int)w;
            cropRect.Height = (int)h;

            return cropRect;
        }

        List<int> positionImages()
        {


            List<int> posID = new List<int>();

            for (int i = 1; i < wCount * 2; i += 2)
            {
                for (int j = 1; j < hCount * 2; j += 2)
                {
                    posID.Add((i * wFormat / (wCount * 2)) - wantedWidth / 2);
                    posID.Add((j * hFormat / (hCount * 2)) - wantedHeight / 2);


                }
            }

            return posID;
        }

        private void selectImage(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();
            if (file.FileName != "" && file.FileName != null)
            {
                filePath = file.FileName;
                pathText.Text = filePath;
                ChangedValue(null, null);
            }
        }

        private void generateImage(object sender, EventArgs e)
        {
            
        }

        private int getWCount()
        {


            if ((float)wFormat / wantedWidth > 2.0f) return 2;

            else if ((float)wFormat / wantedWidth > 1.0f) return 1;
            else return 0;



        }
        private int getHCount()
        {
            if ((float)hFormat / wantedHeight > 2.0f) return 2;

            else if ((float)hFormat / wantedHeight > 1.0f) return 1;

            else return 0;

        }

        protected void PrintImage(object sender, EventArgs e)
        {
            printerName = comboBox3.SelectedItem.ToString();
            
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += PrintPage;
            pd.PrinterSettings.PrinterName = printerName;
            pd.PrinterSettings.Copies = short.Parse( numericUpDown1.Value.ToString());
            pd.DefaultPageSettings.Landscape = false;
            pd.DefaultPageSettings.PaperSize = pd.PrinterSettings.PaperSizes[0];
            pd.DefaultPageSettings.Margins = new Margins {Top = 0 , Bottom = 0, Right = 0, Left = 0 };
            
            pd.OriginAtMargins = false;



            string temppath = string.Format("{0}myLogId_{1}.txt", logPath, DateTime.Now.ToString("yyyy-MM-dd"));
            //pd.PrinterSettings.PrinterName= pd.PrinterSettings.PrinterName[1].ToString();
            Directory.CreateDirectory(logPath);
            if (!File.Exists(string.Format(temppath)))
            {
                File.Create(temppath).Close();
            }

            using (TextWriter tw = new StreamWriter(temppath, true))
            {
                tw.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm") + " : ID Printed : " + filePath);
                tw.Dispose();
                tw.Close();
            }
            //pd.PrinterSettings.PrinterName= pd.PrinterSettings.PrinterName[1].ToString();
                
            pd.Print();
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = idPhoto;
            //Point loc = new Point(100, 100);
            e.Graphics.DrawImage(img, e.MarginBounds);
        }

        private void ListPrinters(string defaultPrinter)
        {
            if (PrinterSettings.InstalledPrinters.Count < 0)
            {
                MessageBox.Show("Nema instaliranih printera!");
                return;
            }
            
            int i;
            for (i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                
                comboBox3.Items.Add(PrinterSettings.InstalledPrinters[i].ToString());
                if (defaultPrinter == "")
                {
                    if (PrinterSettings.InstalledPrinters[i].ToString().ToLower().Contains("selphy"))
                    {
                        comboBox3.Text = PrinterSettings.InstalledPrinters[i].ToString();
                        printerName = PrinterSettings.InstalledPrinters[i].ToString();
                        setDefaultPrinter(printerName);
                        break;
                    }
                }
                else
                {
                    if (string.Compare(PrinterSettings.InstalledPrinters[i].ToString(), defaultPrinter,true) == 0)
                    {
                        comboBox3.Text = PrinterSettings.InstalledPrinters[i].ToString();
                        
                    }
                }
            }
            //if (printerName.Length < 2)
            //{
            //    MessageBox.Show("Selphy printer nije nađen!");
            //    return;
            //}
        }

        private void selectionChanged(object sender, EventArgs e)
        {
            widthText.Enabled = false;
            heightText.Enabled = false;
            switch (comboBox1.SelectedIndex)
            {

                case 0:

                    wantedWidth = 35;
                    wantedHeight = 45;
                    break;
                case 1:
                    wantedWidth = 30;
                    wantedHeight = 35;


                    break;

                case 2:
                    wantedWidth = 40;
                    wantedHeight = 60;
                    break;

                case 3:
                    wantedWidth = 50;
                    wantedHeight = 50;
                    break;
                case 4:
                    widthText.Enabled = true;
                    heightText.Enabled = true;
                    break;
                default:

                    break;
            }
            heightText.TextChanged -= ChangedValue;
            heightText.Text = wantedHeight.ToString();
            widthText.Text = wantedWidth.ToString();
            heightText.TextChanged += ChangedValue;
            
        }

        private void ChangedValue(object sender, EventArgs e)
        {
            int test;
            if (filePath == null) return;
            if (int.TryParse(widthText.Text.Replace(".", "").Replace(",", ""), out test))
            {
                if (test <= 0) return;
                wantedWidth = test;
            }
            else
            {
                return;
            }
            if (int.TryParse(heightText.Text.Replace(".", "").Replace(",", ""), out test))
            {
                if (test <= 0) return;
                wantedHeight = test;
            }
            else
            {
                return;
            }
            
            hCount = getHCount();
            wCount = getWCount();

            if (hCount != 0 && wCount != 0)
                idPhoto = (Image)cropImage();

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Image = idPhoto;
            
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            setDefaultPrinter(comboBox3.Text);
        }

        private string getDefaultPrinter()
        {
            string fullPath = string.Format("{0}config.ini", logPath);
            string printerName = "";
            if (!File.Exists(fullPath))
            {
                try
                {

                    File.Create(fullPath).Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("File writing failed. \n Error: \n {0}{1}", ex.Message, fullPath));
                    this.Dispose();
                    this.Close();
                }

            }
            else
            {

            }
                using (StreamReader tr = new StreamReader(fullPath))
                {
                    string line;
                    while ((line = tr.ReadLine())!= null)
                    {
                        if (line == null || line == "") continue;
                        if (!line.Contains("DefaultPrinter=")) continue;
                        printerName = line.Substring("DefaultPrinter=".Length);
                        return printerName;
                    }

                }
                return printerName;
        }
        private void setDefaultPrinter(string printerName)
        {
            string fullPath = string.Format("{0}/config.ini", logPath);


                using (StreamWriter sw = new StreamWriter(fullPath, false))
                {
                    sw.WriteLine(string.Format("DefaultPrinter={0}", printerName));
                }
                MessageBox.Show(string.Format("{0} je postavljen kao defaultni.", printerName));
            return;
        }
        //private void printImagef(object sender, EventArgs e)
        //{
        //    if (printers.PrinterSettings.InstalledPrinters.Count < 0) {
        //        MessageBox.Show("Nema instaliranih printera!"); 
        //        return;}
        //    string printerName = "";
        //    int i;
        //    for (i = 0; i < printers.PrinterSettings.InstalledPrinters.Count; i++ )
        //    {
        //        if (printers.PrinterSettings.InstalledPrinters.ToString().ToLower().Contains("selphy"))
        //        {
        //            printerName = printers.PrinterSettings.InstalledPrinters[i].ToString();
        //            break;
        //        }

        //    }
        //    if (printerName.Length > 0)
        //    {
        //        idPhoto.Save("C:\\temp.jpeg");
        //        printers.PrintDocument print = new printers.PrintDocument();
        //        print.PrintPage += "C:\\temp.jpeg";
        //        print.PrinterSettings.PrinterName = printerName;
        //        print.Print();
        //    }
        //    else {
        //        idPhoto.Save("C:\\temp.jpeg");
        //        PrintDialog pDiag = new PrintDialog();
        //        pDiag.Document.DocumentName = "C:\\temp.jpeg";
        //        pDiag.ShowDialog();
        //}
        //}
    }
}
